#!/usr/bin/env python

from distutils.core import setup

setup(name='airtabledb',
      version='0.4',
      description='AirtableDB',
      author='Donald Strubler',
      author_email='@infintropy',
      packages=['airtabledb'],
      install_requires=[
        'airtable-python-wrapper',
        'nanoid',
    ],
     )