import airtable
import nanoid
from .base import Base


def make_id():
    return nanoid.generate(
        "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    )

def get_airtabledb(name=None, base=None, api_key=None) -> Base:

    base_code = base
    # return base_code
    # return type(base_code)
    return Base(base_code, api_key=api_key)
