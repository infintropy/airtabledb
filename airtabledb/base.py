import airtable
import nanoid
from .table import Table


class Base:
    def __init__(self, base, api_key):
        """
        Object representing the "Base" organizational aspect of AirTable
        A "Base" is a collection of Tables. 

        By making a base, you have access to request any table in the base by name. 

        Args:
            base (str): Name of the Base instance being created
        """

        self._table = None
        self._tables = {}

        self._records = {}

        self._base_code = base
        self._at_api_key = api_key

    def table(self, table_name: str) -> Table:
        """
        Get a table object from the base. The table object is registered as a
        lowercase attribute with space delimiters replaced with underscore.

        Args:
            table_name: string of the table name as it appears in AirTable base.

        Returns:
            Table: new instance of table object.

        """
        # make table
        table = Table(table_name, self)

        setattr(self, "_".join(table_name.lower().split()), table)
        return table

    @property
    def records(self):
        # get refreshed records from current table
        return self._records
