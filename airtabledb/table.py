import airtable
from .record import Record


class Table:
    _airtable = None
    _records = {}

    def __init__(self, name: str, base=None) -> None:
        self._records = {}
        self.name = name
        self.base = base

    def __getitem__(self, key):
        return self.record(key)

    def record(self, id):
        if not self._records:
            self.records
        if id in self._records.keys():
            return self._records[id]

    @property
    def airtable(self):
        if not self._airtable:
            self._airtable = airtable.Airtable(
                self.base._base_code, self.name, self.base._at_api_key
            )
        return self._airtable

    @property
    def records(self):
        # get refreshed records from current table
        records = self.airtable.get_all()
        for record in records:
            rec = Record(record, self)
            self._records[record.get("id")] = rec
            # self.base._records[record.get("id")] = rec

        return [i[1] for i in list(self._records.items())]

    def insert(self, document):
        if type(document) is dict:
            record = self.airtable.insert(document, typecast=True)
            return Record(record, self)

        elif type(document) is list:
            records = self.airtable.batch_insert(document, typecast=True)
            return [Record(record, self) for record in records]

    def update(self, id, document):
        self.airtable.update(id, document)
