class Record:

    _table = None
    _contents = {}

    def __init__(self, contents: dict, table) -> None:
        """
        Args:
            contents (dict): _description_
            table (_type_): _description_
        """
        self.contents = contents
        self._table = table

    def __getitem__(self, key):
        val = self.fields.get(key)
        if type(val) is list and len(val) is 1 and type(val[0]) in [str, int, float]:
            if type(val[0]) is str and not val[0].startswith("rec"):
                val = val[0]
            else:
                val = val[0]
        return val

    def __repr__(self):
        return f"{self.table.name}[{self.id}]"

    @property
    def id(self):
        return self._contents.get("id")

    @property
    def fields(self):
        return self._contents.get("fields")

    @property
    def contents(self):
        return self._contents

    @contents.setter
    def contents(self, contents):
        if contents.get("id"):
            self._contents = contents
        else:
            raise KeyError(
                "Key: 'id' required within the incoming document per Airtable schema."
            )

    @property
    def table(self):
        return self._table

    @property
    def base(self):
        return self._table.base

    def update(self, values: dict):
        rec = self.table.airtable.update(self.id, values)
        return rec
